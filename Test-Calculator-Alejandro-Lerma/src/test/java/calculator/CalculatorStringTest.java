package calculator;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CalculatorStringTest {

	
	@Test
    public void calculateEmptyResult() {
        Assert.assertEquals(0, CalculatorStringClass.add(""));
    }

    @Test
    public void calculateSingleNumber() {
        Assert.assertEquals(1, CalculatorStringClass.add("1"));
    }

    @Test
    public void calculateTwo_expectTwo() {
        Assert.assertEquals(2, CalculatorStringClass.add("2"));
    }

    @Test
    public void calculateTwoToOne_expectThree() {
        Assert.assertEquals(3, CalculatorStringClass.add("2,1"));
    }

    @Test
    public void calculateTwoToOneWithNewlines_expectThree() {
        Assert.assertEquals(3, CalculatorStringClass.add("2\n1"));
    }

    @Test
    public void calculateTwoToOneWithCustomDelimiter_expectThree() {
        Assert.assertEquals(3, CalculatorStringClass.add("//[x]\n2x1"));
    }

    @Test
    public void calculateTwoToOneWithCustomDelimiterWithLengthTwo_expectThree() {
        Assert.assertEquals(3, CalculatorStringClass.add("//[xx]\n2xx1"));
    }

    @Test
    public void calculateTwoToOneToThreeWithTwoCustomDelimiter_expectThree() {
        Assert.assertEquals(6, CalculatorStringClass.add("//[x][y]\n2x1y3"));
    }

    @Test
    public void calculateSingleNegativeValue_exception() {
        try {
            CalculatorStringClass.add("-1");
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("negatives not allowed (-1)", e.getMessage());
        }
    }

    @Test
    public void calculateTwoNegativeValue_exception() {
        try {
            CalculatorStringClass.add("-1,-2");
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("negatives not allowed (-1,-2)", e.getMessage());
        }
    }

    @Test
    public void calculateTwoToOneThousandOne_Two() {
        Assert.assertEquals(2, CalculatorStringClass.add("2,1001"));
    }
}
